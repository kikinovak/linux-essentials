# Évolution de Linux et systèmes d'exploitation populaires

## Compétences

- Distributions

- Systèmes embarqués

- Linux dans le cloud

## Termes

- Debian, Ubuntu (LTS)

- CentOS, OpenSUSE, Red Hat, SUSE

- Linux Mint, Scientific Linux

- Raspberry Pi, Raspbian

- Android

## Documentation

- [1.1 Lesson
  1](https://learning.lpi.org/en/learning-materials/010-160/1/1.1/1.1_01/)
  (english)

- [1.1 Leçon
  1](https://learning.lpi.org/fr/learning-materials/010-160/1/1.1/1.1_01/)
  (français)

## Vidéos

- [Linux Essentials - Shawn
  Powers](https://www.youtube.com/watch?v=skTShEHyXfo)

- [What is Linux and what is a distro ? - Shawn
  Powers](https://www.youtube.com/watch?v=meAGfhD3_ww)


